# -*- coding: utf-8 -*-

import sys

sys.path.insert(0, '/home/p3/dom')

activate_this = '/home/p3/venv/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

from bottle_blog import app as application