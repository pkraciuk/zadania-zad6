
import os
import sqlite3

db_filename = 'books.db'
schema_filename = 'schema.sql'

db_is_new = not os.path.exists(db_filename)

with sqlite3.connect(db_filename) as conn:
    if db_is_new:
        print 'Createing schema'
        with open(schema_filename, 'rt') as f:
            schema = f.read()
        conn.executescript(schema)
        print 'Inserting initial data'
        conn.execute("""
        insert into books values ('1','CherryPy Essentials: Rapid Python Web Application Development','978-1904811848','Packt Publishing (March 31, 2007)','Sylvain Hellegouarch')
        """)
        conn.execute("""
        insert into books values ('2','Python for Software Design: How to Think Like a Computer Scientist','978-0521725965','Cambridge University Press; 1 edition (March 16, 2009)','Allen B. Downey')
        """)

        conn.execute("""
        insert into books values ('3','Foundations of Python Network Programming','978-1430230038','Apress; 2 edition (December 21, 2010)','John Goerzen')
        """)

        conn.execute("""
        insert into books values ('4','Python Cookbook, Second Edition','978-0-596-00797-3','O''Reilly Media','Alex Martelli, Anna Ravenscroft, David Ascher')
        """)
        conn.execute("""
        insert into books values ('5','The Pragmatic Programmer: From Journeyman to Master','978-0201616224','Addison-Wesley Professional (October 30, 1999)','Andrew Hunt, David Thomas')
        """)
    else:
        print 'Database exists, assume schema does too'
conn.close()

