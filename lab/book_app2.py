# -*- coding: utf-8 -*-
import sqlite3

from flask import Flask, render_template
from flask import g
from contextlib import closing

DATABASE = 'books.db'

app = Flask(__name__)
app.config.from_object(__name__)


@app.route('/')
def books():
    # Przekaż listę książek do szablonu "book_list.html"
    entries = titles()
    return render_template('book_list.html', entries=entries)
    pass


@app.route('/book/<book_id>/')
def book(book_id):
    print book_id
    entries = title_info(book_id)
    return render_template('book_detail.html', entries=entries)
    pass

###############
# Baza danych #
###############

def connect_db():
    return sqlite3.connect(app.config['DATABASE'])


def init_db():
    with closing(connect_db()) as db:
        with app.open_resource('schema.sql') as f:
            db.cursor().executescript(f.read())
        db.commit()


@app.before_request
def before_request():
    g.db = connect_db()


@app.teardown_request
def teardown_request(exception):
    g.db.close()

def titles():
    cur = g.db.execute('select id, title from books order by id desc')
    # Stworzenie słownika dla każdego odczytanego wiersza oraz upakowanie ich w listę
    entries = [dict(id=row[0], title=row[1]) for row in cur.fetchall()]
    return entries

def title_info(book_id):
    cur = g.db.execute('select title, isbn, publisher, author from books where id = '+book_id)
    # Stworzenie słownika dla każdego odczytanego wiersza oraz upakowanie ich w listę
    row = cur.fetchall()[0]
    entries = dict(title=row[0], isbn=row[1], publisher=row[2], author=row[3])
    return entries

if __name__ == '__main__':
    #app.run(debug=True,host='194.29.175.240')
    app.run(debug=True)