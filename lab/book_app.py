# -*- coding: utf-8 -*-

from flask import Flask, render_template
import bookdb

app = Flask(__name__)

db = bookdb.BookDB()


@app.route('/')
def books():
    # Przekaż listę książek do szablonu "book_list.html"
    entries = bookdb.BookDB().titles()
    return render_template('book_list.html', entries=entries)
    pass


@app.route('/book/<book_id>/')
def book(book_id):
    print book_id
    entries2 = bookdb.BookDB().title_info(book_id)
    return render_template('book_detail.html', entries=entries2)
    pass


if __name__ == '__main__':
    app.run(debug=True,host='194.29.175.240')