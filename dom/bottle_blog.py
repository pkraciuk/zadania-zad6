import sqlite3
from bottle import route, run, request, post, get, response
import bottle

bottle.debug(True)
@route('/')
def show_entries():
    conn = sqlite3.connect('microblog.db')
    c = conn.cursor()
    c.execute("SELECT * FROM entries")
    result = c.fetchall()
    html= """
    <h1 style="color:#377BA8; font-family: 'Georgia',serif;margin:0;border-bottom:2px solid #eee;">Bottle blog</h1>
    """
    username = request.get_cookie("account", secret='very-secret-key')
    if username:
        html+="""
        <nav style="text-align: right; font-size: 0.8em; padding: 0.3em;margin-bottom: 1em; background: #fafafa;">
            <a style="color:#377BA8" href="/logout">Logout</a></br>
        </nav>

        """
    else:
        html+="""
        <nav style="text-align: right; font-size: 0.8em; padding: 0.3em;margin-bottom: 1em; background: #fafafa;">
            <a style="color:#377BA8" href="/login">Login</a></br>
        </nav>
        """
    html+="""
    <h2 style="color:#377BA8; font-family: 'Georgia', serif; margin: 0;font-size: 1.6em;">Entries:</h2>
    <ul style="list-style: none; margin: 0; padding: 0;">
    """
    for row in result:
        html+= '<li style="margin: 0.8em 1.2em;"><h2 style="color: #377BA8; margin: 0; font-size: 1.2em;margin-left: -1em;">'+str(row[0])+": "+str(row[1])+'</h2><div>'+str(row[2])+'</div></li>'
    html+='</ul><hr/>'
    if username:
        html+='<a style="color:#377BA8" href="/add">Add post</a></br>'
    return html


@route('/logout')
def logout():
    response.delete_cookie("account", secret='very-secret-key')
    return """
    <h1 style="color:#377BA8; font-family: 'Georgia',serif;margin:0;border-bottom:2px solid #eee;">Bottle blog</h1>
    <nav style="text-align: right; font-size: 0.8em; padding: 0.3em;margin-bottom: 1em; background: #fafafa;">
        <a style="color:#377BA8" href="/">Return</a>
    </nav>
    <p style="color:#377BA8">Now you are logged out</p>

    """

@route('/add')
def add_entry():
    return """
    <h1 style="color:#377BA8; font-family: 'Georgia',serif;margin:0;border-bottom:2px solid #eee;">Bottle blog</h1>
    <nav style="text-align: right; font-size: 0.8em; padding: 0.3em;margin-bottom: 1em; background: #fafafa;">
        <a href="/">Return</a>
    </nav>
    <form method="POST" action="/added">
                <input name="input" type="text" />
                <input type="submit" value="Send" />
              </form>
              """

@post('/added')
def added_entry():
    html ="""<h1 style="color:#377BA8; font-family: 'Georgia',serif;margin:0;border-bottom:2px solid #eee;">Bottle blog</h1>
    <nav style="text-align: right; font-size: 0.8em; padding: 0.3em;margin-bottom: 1em; background: #fafafa;">
        <a href="/">Return</a>
    </nav>
    <p style="color:#377BA8">You have added: </p><p>"""
    html+=request.forms.get('input')
    html+="</p>"
    conn = sqlite3.connect('microblog.db')
    c=conn.cursor()
    query = "insert into entries (title, text) values ('"+request.get_cookie("account", secret='very-secret-key')+"','"+request.forms.get('input')+"')"
    c.execute(query)
    conn.commit()
    c.close()
    return html

@get('/login') # or @route('/login')
def login_form():
    return """
    <h1 style="color:#377BA8; font-family: 'Georgia',serif;margin:0;border-bottom:2px solid #eee;">Bottle blog</h1>
    <nav style="text-align: right; font-size: 0.8em; padding: 0.3em;margin-bottom: 1em; background: #fafafa;">
        <a style="color:#377BA8" href="/">Return</a>
    </nav>
    <form method="POST" action="/login">
                <input name="name"     type="text" />
                <input name="password" type="password" />
                <input type="submit" value="Log in" />
              </form>

              """

#Logowanie

@post('/login') # or @route('/login',method='POST')
def login_submit():
    name = request.forms.get('name')
    password = request.forms.get('password')
    if check_login(name, password):
        response.set_cookie("account",name,secret='very-secret-key')
        return """
        <h1 style="color:#377BA8; font-family: 'Georgia',serif;margin:0;border-bottom:2px solid #eee;">Bottle blog</h1>
        <nav style="text-align: right; font-size: 0.8em; padding: 0.3em;margin-bottom: 1em; background: #fafafa;">
            <a style="color:#377BA8" href="/">Return</a>
        </nav>
        <p style="color:#377BA8">Your login was correct</p>
        """

    else:
        return """
        <h1 style="color:#377BA8; font-family: 'Georgia',serif;margin:0;border-bottom:2px solid #eee;">Bottle blog</h1>
        <nav style="text-align: right; font-size: 0.8em; padding: 0.3em;margin-bottom: 1em; background: #fafafa;">
            <a href="/">Return</a>
        </nav>
        <p style="color:#377BA8">Login failed</p>
        """

def check_login(name, password):
    if name=='a' and password=='a':
        return True
    elif name=='Guest' and password =='123':
        return True
    elif name=='Student' and password =='123':
            return True
    elif name=='User' and password =='123':
        return True
    else:
        return False


#run(host='localhost', port=8080)
run(host='194.29.175.240',port = 31003)